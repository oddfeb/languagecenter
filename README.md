# README

A rails web application that i plan to expand over time into a fully 
functional online testing system. The application will allow following features:
 1. A pool of MCQs (Multiple Choice Questions) on various subjects
 2. Authentication 
 3. Authorization at least two roles in mind admin and non-admin (users)
 4. Generation of Test on a given subject by randomly selecting questions from the pool
 5. Enrollment of users for a test meeting a set criteria
 6. Persisted record of attempts by users
 7. Evaluation of users
 8. A dashboard to examin various statistics

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
